import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentFolderComponent } from './payment-folder.component';

describe('PaymentFolderComponent', () => {
  let component: PaymentFolderComponent;
  let fixture: ComponentFixture<PaymentFolderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentFolderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
