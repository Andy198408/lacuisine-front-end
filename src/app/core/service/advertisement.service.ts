import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Advertisement} from '../model/advertisement';
import {AdvertisementCollection} from '../model/advertisement-collection';

/**
 * Created by "Andy Van Camp" on 16/08/2021.
 */
const API_URL = 'http://localhost:8080/api/advertisement';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AdvertisementService {

  constructor(private httpClient: HttpClient) {
  }

  getAdvertisementById(id: number): Observable<Advertisement> {
    return this.httpClient.get<Advertisement>(API_URL + '/' + id);
  }

  getAllAdvertisement(): Observable<AdvertisementCollection> {
    return this.httpClient.get<AdvertisementCollection>(API_URL);
  }

  saveAdvertisement(advertisement: Advertisement, cost: number): Observable<Advertisement> {
    return this.httpClient.post<Advertisement>(API_URL + '?cost=' + cost, advertisement, httpOptions);
  }

  updateAdvertisement(advertisement: Advertisement, cost: number): Observable<Advertisement> {
    return this.httpClient.put<Advertisement>(API_URL + '/' + advertisement.id + '/?cost=' + cost, advertisement, httpOptions);
  }

  updateAllAdvertisement(advertisementCollection: AdvertisementCollection): Observable<AdvertisementCollection> {
    return this.httpClient.put<AdvertisementCollection>(API_URL, advertisementCollection, httpOptions);
  }

  deleteById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }


}
