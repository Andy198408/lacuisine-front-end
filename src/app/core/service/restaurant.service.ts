/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RestaurantCollection} from '../model/restaurant-collection';
import {Restaurant} from '../model/restaurant';

const API_URL = 'http://localhost:8080/api/restaurant';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private httpClient: HttpClient) {
  }

  getRestaurantById(id: number): Observable<Restaurant> {
    return this.httpClient.get<Restaurant>(API_URL + '/' + id);
  }

  getAllRestaurant(): Observable<RestaurantCollection> {
    return this.httpClient.get<RestaurantCollection>(API_URL);
  }

  getAllRestaurantByUserId(userId: number): Observable<RestaurantCollection> {
    return this.httpClient.get<RestaurantCollection>(API_URL + '?userId=' + userId);
  }

  updateRestaurant(id: number, resto: Restaurant): Observable<Restaurant> {
    return this.httpClient.put<Restaurant>(API_URL + '/' + id, resto, httpOptions);
  }

  saveRestaurant(resto: Restaurant): Observable<Restaurant> {
    return this.httpClient.post<Restaurant>(API_URL, resto, httpOptions);
  }

  deleteRestaurantById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }


}
