/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserCollection} from '../model/user-collection';

const API_URL = 'http://localhost:8080/api/user/';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  getAllUsers(): Observable<UserCollection> {
    return this.httpClient.get<UserCollection>(API_URL);
  }

  deleteUser(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + id);
  }

}
