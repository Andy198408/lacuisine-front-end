/**
 * Created by "Dorien and Nick" on 12/08/2021.
 */

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MenuItemCollection} from '../model/menu-item-collection';
import {MenuItem} from '../model/menuItem';
import {LineGraphFormat} from '../model/line-graph-format';
import {PieChartFormat} from '../model/pie-chart-format';

const API_URL = 'http://localhost:8080/api/menuitem';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MenuItemService {

  constructor(private httpClient: HttpClient) {
  }

  findAllMenuItemsByRestaurantId(restaurantId: number): Observable<MenuItemCollection> {
    return this.httpClient.get<MenuItemCollection>(API_URL + '?menuId=' + restaurantId);
  }

  findLineGrapDataByRestaurantId(restaurantId: number): Observable<Array<LineGraphFormat>> {
    return this.httpClient.get<Array<LineGraphFormat>>(API_URL + '/linegraph/' + restaurantId);
  }

  findPieChartGrapDataByRestaurantId(restaurantId: number, type: string): Observable<Array<PieChartFormat>> {
    return this.httpClient.get<Array<PieChartFormat>>(API_URL + '/piechart/' + restaurantId + '?type=' + type);
  }

  updateMenuItem(id: number, menuItem: MenuItem): Observable<MenuItem> {
    return this.httpClient.put<MenuItem>(API_URL + '/' + id, menuItem, httpOptions);
  }

  saveMenuItems(menuItem: MenuItem[]): Observable<MenuItem[]> {
    return this.httpClient.post<MenuItem[]>(API_URL, menuItem, httpOptions);
  }

  deleteMenuItemById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }
}
