/**
 * Created by "Dorien and Nick" on 17/08/2021.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OrderItem} from '../model/orderItem';
import {OrderItemCollection} from '../model/orderItem-collection';
import {Restaurant} from '../model/restaurant';
import {Order} from '../model/order';

const API_URL = 'http://localhost:8080/api/orderitem';
const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class OrderItemService {

  constructor(private httpClient: HttpClient) {
  }

  getOrderItemById(id: number): Observable<OrderItem> {
    return this.httpClient.get<OrderItem>(API_URL + '/' + id);
  }

  getAllOrderItem(orderItemCollection: OrderItem[]): Observable<OrderItemCollection> {
    return this.httpClient.get<OrderItemCollection>(API_URL);
  }

  updateOrderItem(orderItems: OrderItemCollection): Observable<OrderItemCollection> {
    return this.httpClient.put<OrderItemCollection>(API_URL + '/' + orderItems, httpOptions);
  }

  saveOrderItems(orderItems: OrderItemCollection): Observable<OrderItemCollection> {
    return this.httpClient.post<OrderItemCollection>(API_URL, orderItems, httpOptions);
  }

  deleteOrderItemById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }

  getOrderItemsByOrderId(orderId: number): Observable<OrderItemCollection> {
    console.log('trigger get orderitems for: ' + orderId)
    return this.httpClient.get<OrderItemCollection>(API_URL + '?orderItemId=' + orderId);
  }
}
