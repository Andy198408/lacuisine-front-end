import {Injectable} from '@angular/core';
import {Order} from '../model/order';

const CURRENT_ORDER = 'current-order';

@Injectable({providedIn: 'root'})
export class SessionDataStorageService {
  public saveorder(order: Order): void {
    window.sessionStorage.setItem(CURRENT_ORDER, JSON.stringify(order));
  }

  public getCurrentorder(): Order {
    return JSON.parse(window.sessionStorage.getItem(CURRENT_ORDER));
  }

  clearOrder(): void {
    window.sessionStorage.removeItem(CURRENT_ORDER);
  }
}
