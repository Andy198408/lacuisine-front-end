/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
import {Wallet} from './wallet';

export class WalletCollection {
  walletCollection: Wallet[];
}
