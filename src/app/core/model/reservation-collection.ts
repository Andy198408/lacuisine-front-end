import {Reservation} from './reservation';

/**
 * Created by "Andy Van Camp" on 25/08/2021.
 */
export class ReservationCollection {
  reservationCollection: Reservation[];
}
