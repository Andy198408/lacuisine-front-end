/**
 * shiwi & oli.
 */

import {OrderItem} from './orderItem';

export class OrderItemCollection {
  orderItemCollection: OrderItem[] = [];
}
