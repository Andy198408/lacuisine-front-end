/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Restaurant} from './restaurant';

export class Address {
  id: number;
  street: string;
  nr: number;
  zipcode: number;
  city: string;
  country: string;
  restaurant: Restaurant;
}
