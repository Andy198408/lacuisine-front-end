/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Address} from './address';
import {User} from './user';

export class Restaurant {
  id: number;
  name: string;
  description: string;
  address: Address;
  kitchen: string;
  parking: boolean;
  owner: User;
  earnings: number;

  open = false;
  rating: number;


}
