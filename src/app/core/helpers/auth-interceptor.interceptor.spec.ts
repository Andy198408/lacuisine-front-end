import {AuthInterceptor} from './auth-interceptor.interceptor';
import {TokenStorageService} from '../service/token-storage.service';

describe('AuthInterceptor', () => {
  it('should create an instance', () => {
    expect(new AuthInterceptor(new TokenStorageService())).toBeTruthy();
  });
});
