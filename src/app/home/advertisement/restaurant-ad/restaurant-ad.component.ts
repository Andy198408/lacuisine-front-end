import {Component, Input} from '@angular/core';
import {AdComponent} from '../../../shared/ad/ad-component';
import {Advertisement} from '../../../core/model/advertisement';

@Component({
  template: `
    <div class="card "
         routerLink="/restaurant/{{data.restaurant.id}}">
      <img src="{{imgSource}}" class="card-img-top" alt="...">
      <div class="card-body ">
        <h3>{{ data.restaurant.name }}</h3>
        <p class="card-text">{{data.description}}</p>
      </div>
    </div>
  `,
  styleUrls: ['./restaurant-ad.component.css']
})
export class RestaurantAdComponent implements AdComponent {

  @Input() data: Advertisement;
  imgSource = 'assets/img/default.jpg';


}
