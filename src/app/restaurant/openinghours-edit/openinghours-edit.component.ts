import {Component, OnInit} from '@angular/core';
import {OpeninghoursService} from '../../core/service/openinghours.service';
import {Openinghours} from '../../core/model/openinghours';
import {Location} from '@angular/common';
import {Restaurant} from '../../core/model/restaurant';
import {ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

/**
 * Created by "Andy Van Camp" on 18/08/2021.
 */
@Component({
  selector: 'app-openinghours-edit',
  templateUrl: './openinghours-edit.component.html',
  styleUrls: ['./openinghours-edit.component.css']
})
export class OpeninghoursEditComponent implements OnInit {

  openingHourForm: FormGroup;

  openinghours: Openinghours = new Openinghours();

  openinghoursCollection: Openinghours[];

  restaurant: Restaurant = new Restaurant();

  isSuccessful = false;
  isFailed = false;
  errorMessage = '';
  error = false;
  isNew = true;
  days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];

  constructor(private openinghoursService: OpeninghoursService,
              private location: Location,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.openingHourForm = new FormGroup({
      id: new FormControl(null),
      restaurant: new FormControl(null),
      weekday: new FormControl(null, [Validators.required]),
      openTime: new FormControl(null, [Validators.required]),
      closeTime: new FormControl(null, [Validators.required]),
      reservationTimeslot: new FormControl(null, [Validators.required, Validators.min(15), Validators.max(60)]),
      active: new FormControl(false),
    });
    this.restaurant.id = Number(this.route.snapshot.paramMap.get('id'));
    this.openinghoursService.getAllOpeninghoursByRestaurantId(this.restaurant.id).subscribe({
      next: data => this.openinghoursCollection = data.openinghoursCollection
    });
  }

  onSubmit(): void {
    if (this.openingHourForm.get('openTime').value >= this.openingHourForm.get('closeTime').value) {
      this.isFailed = true;
      this.errorMessage = 'opening time must be before closing';
    } else {
      if (confirm('wanne save?')) {
        // saving openinghour
        if (this.openingHourForm.value.id === undefined || this.openingHourForm.value.id === null) {
          this.openinghours = this.openingHourForm.value;
          this.openinghours.restaurant = this.restaurant;
          this.openinghoursService.saveOpeninghours(this.openinghours).subscribe({
            error: error1 => this.errorMessage = error1.errorMessage,
            complete: () => {
              console.log('trigger');
              this.ngOnInit();
            }
          });
        } else {
          // updating openinghour
          this.openinghours = this.openingHourForm.value;
          this.openinghours.restaurant = this.restaurant;
          this.openinghoursService.updateOpeninghours(this.openinghours).subscribe({
            error: error1 => this.errorMessage = error1.errorMessage,
            complete: () => this.ngOnInit()
          });
        }
      }
    }
  }

  goBack(): void {
    this.location.back();
  }

  deleteOpeninghour(): void {
    this.openinghoursService.deleteOpeninghoursById(this.openinghours.id).subscribe({
      complete: () => this.ngOnInit()
    });
  }

  setForUpdate(openinghour: Openinghours): void {
    this.restaurant = openinghour.restaurant;
    this.isNew = false;
    this.openingHourForm.setValue(openinghour);
    this.openinghours.id = openinghour.id;
  }

  setNew(): void {
    this.openinghours = new Openinghours();
    this.isSuccessful = false;
    this.isNew = true;
    this.openingHourForm.reset();

  }


}
