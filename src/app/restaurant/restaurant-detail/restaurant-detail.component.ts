/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RestaurantService} from '../../core/service/restaurant.service';
import {Restaurant} from '../../core/model/restaurant';
import {Location} from '@angular/common';
import {TokenStorageService} from '../../core/service/token-storage.service';
import {User} from '../../core/model/user';
import {MenuItem} from '../../core/model/menuItem';
import {MenuItemService} from '../../core/service/menu-item.service';
import {Menu} from '../../core/model/menu';
import {OrderItem} from '../../core/model/orderItem';
import {Order} from '../../core/model/order';
import {OrderService} from '../../core/service/order.service';
import {OrderItemService} from '../../core/service/order-item.service';
import {OrderItemCollection} from '../../core/model/orderItem-collection';
import {OpeninghoursService} from '../../core/service/openinghours.service';
import {Openinghours} from '../../core/model/openinghours';
import {AddressService} from '../../core/service/address.service';
import {Address} from '../../core/model/address';
import {RatingCollection} from '../../core/model/rating-collection';
import {RatingService} from '../../core/service/rating.service';
import {Rating} from '../../core/model/rating';
import {forkJoin} from 'rxjs';
import {SessionDataStorageService} from "../../core/service/session-data-storage.service";

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  styleUrls: ['./restaurant-detail.component.css']
})
export class RestaurantDetailComponent implements OnInit {

  currentOrder: Order = new Order();

  restaurant: Restaurant = new Restaurant();
  address: Address;

  isSuccessful = false;
  isFailed = false;
  errorMessage: '';

  error = false;

  currentUser: User;

  currentUserRoles: string[];
  ratingCollection: RatingCollection = new RatingCollection();
  rating: number;
  rating2: Rating = new Rating();

  showOrder = false;
  showEdit = false;
  // showMenu = false;
  isAdmin = false;
  showOrderList = false;
  menuItems: MenuItem[];
  orderItems: OrderItemCollection = new OrderItemCollection();

  menuItem: MenuItem = new MenuItem();
  orderItem: OrderItem = new OrderItem();

  // todo delete 2 lines ?
  foodTypes = ['DRINK', 'FOOD'];
  menu: Menu = new Menu();

  openinghoursCollection: Openinghours[];
  openingMap = new Map();
  sortedMap = new Map();
  days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
  childActive = false;
  isLoaded = false;

  constructor(private route: ActivatedRoute,
              private location: Location,
              private restaurantService: RestaurantService,
              private tokenStorage: TokenStorageService,
              private orderService: OrderService,
              private orderItemService: OrderItemService,
              private menuItemService: MenuItemService,
              private router: Router,
              private openinghoursService: OpeninghoursService,
              private addressService: AddressService,
              private ratingService: RatingService,
              private storageService: SessionDataStorageService,) {
  }

  ngOnInit(): void {
    this.resetData();
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    if (restaurantId !== 0) {
      this.intitiateData(restaurantId);
    }
    const currentorder = this.storageService.getCurrentorder();
    if (currentorder) {
      if (currentorder.restaurant.id !== restaurantId) {
        this.storageService.clearOrder();
      }
      this.orderItems.orderItemCollection = this.storageService.getCurrentorder().orderItemList;
    }
    if (this.orderItems.orderItemCollection.length > 0) {
      this.showOrderList = true;
    }
  }


  private setPermission(): void {
    // loading user
    const user = this.tokenStorage.getUser();
    this.currentUserRoles = user.roles;
    this.currentUser = user;
    if (this.currentUserRoles) {
      // todo delete role check on admin and mod since everyone is user!!
      this.showOrder = this.currentUserRoles.includes('ROLE_USER');
      // this.showMenu = this.currentUserRoles.includes('ROLE_USER');

      this.showEdit = this.currentUserRoles.includes('ROLE_ADMIN');
      this.isAdmin = this.currentUserRoles.includes('ROLE_ADMIN');
      if (this.currentUserRoles.includes('ROLE_MODERATOR') && this.showEdit === false) {
        this.showEdit = this.restaurant.owner.id === user.id;
      }
    }
  }

  goBack(): void {
    this.location.back();
  }

  addToOrderList(menuItem: MenuItem): void {
    this.showOrderList = true;
    this.orderItem.menuItem = menuItem;
    const index = this.orderItems.orderItemCollection.findIndex(obj => obj.menuItem === menuItem);
    if (index === -1) {
      this.orderItem.amount = 1;
      this.orderItems.orderItemCollection.push(this.orderItem);
    } else {
      this.orderItems.orderItemCollection[index].amount++;
    }
    this.orderItem = new OrderItem();
  }

  saveOrder(): void {
    this.currentOrder.user.id = this.currentUser.id;
    this.currentOrder.restaurant = this.restaurant;
    this.currentOrder.orderItemList = this.orderItems.orderItemCollection;
    this.storageService.saveorder(this.currentOrder);
    this.router.navigate(['./order']);
  }


  deleteFromOrderList(orderItem: OrderItem): void {
    const id = this.orderItems.orderItemCollection.indexOf(orderItem);
    this.orderItems.orderItemCollection[id].amount = this.orderItems.orderItemCollection[id].amount - 1;
    if (this.orderItems.orderItemCollection[id].amount === 0) {
      this.orderItems.orderItemCollection.splice(id, 1);
    }
    if (this.orderItems.orderItemCollection.length === 0) {
      this.showOrderList = false;
    }
  }

  private mapOpenings(): void {
    this.openinghoursCollection.forEach(
      opening => {
        if (opening.active) {
          if (this.openingMap.has(opening.weekday)) {
            const dayKey = this.openingMap.get(opening.weekday);
            dayKey.push(opening);
          } else {
            const openingsList: Openinghours[] = [];
            openingsList.push(opening);
            this.openingMap.set(opening.weekday, openingsList);
          }
        }
      }
    );
  }

  private mapSort(): void {
    const sortedMap = new Map();
    this.days.forEach(day => {
      if (this.openingMap.get(day)) {
        const sortedHours = this.openingMap.get(day).sort((a, b) => (a.openTime > b.openTime) ? 1 : ((a.openTime < b.openTime) ? -1 : 0));
        sortedMap.set(day, sortedHours);
      }
    });
    this.sortedMap = sortedMap;
  }

  returnZero(): number {
    return 0;
  }

  showRestaurantRating(): void {
    if (this.ratingCollection.ratingCollection.length !== 0) {
      this.rating = 0;
      this.ratingCollection.ratingCollection.forEach(rating => this.rating = (this.rating + rating.score));
      this.rating = this.rating / this.ratingCollection.ratingCollection.length;
    } else {
      this.rating = 0;
    }
    this.restaurant.rating = this.rating;
  }


  saveReview(): void {
    this.rating2.restaurant.id = this.restaurant.id;
    this.rating2.user.id = this.currentUser.id;
    this.ratingService.saveRating(this.rating2).subscribe(
      data => {
      },
      error => {
        this.errorMessage = error.error.message;
      },
      () => {
        window.location.reload();
      });
  }

  deleteReview(rating: Rating): void {
    this.ratingService.deleteRating(rating.id).subscribe(
      data => {
      },
      error => {
        this.errorMessage = error.error.message;
      },
      () => {
        window.location.reload();
      }
    );

  }

  private intitiateData(restaurantId: number): void {
    const obs = forkJoin(
      {
        restaurant: this.restaurantService.getRestaurantById(restaurantId),
        adress: this.addressService.getAddressById(restaurantId),
        openinghours: this.openinghoursService.getAllOpeninghoursByRestaurantId(restaurantId),
        menuItems: this.menuItemService.findAllMenuItemsByRestaurantId(restaurantId),
        rating: this.ratingService.getRatingByRestaurantId(restaurantId)
      }
    );

    obs.subscribe({
      next: arr => {
        this.restaurant = arr.restaurant;
        this.address = arr.adress;
        this.openinghoursCollection = arr.openinghours.openinghoursCollection;
        this.menuItems = arr.menuItems.menuItemCollection;
        this.ratingCollection = arr.rating;
      },
      error: err => this.router.navigateByUrl('/404pagenotfound'),
      complete: () => {
        this.setPermission();
        this.showRestaurantRating();
        this.mapOpenings();
        this.mapSort();
        this.menu.id = restaurantId;
        this.menuItem.menu = this.menu;
        this.isLoaded = true;
      }
    });
  }

  setChildActive(): void {
    this.childActive = true;
  }

  setChildNotActive(): void {
    this.childActive = false;
    this.ngOnInit();
  }

  private resetData(): void {
    this.openingMap.clear();
  }
}
