import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {OrderPipe} from './pipes/order.pipe';
import {AdDirective} from './directive/ad.directive';
import {AdBannerComponent} from './ad/ad-banner/ad-banner.component';

@NgModule({
  declarations: [OrderPipe, AdDirective, AdBannerComponent],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxChartsModule,
    MatDialogModule,
    MatIconModule,

  ],
  exports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxChartsModule,
    MatDialogModule,
    MatIconModule,
    OrderPipe,
    AdDirective,
    AdBannerComponent

  ]
})

export class SharedModule {
}
