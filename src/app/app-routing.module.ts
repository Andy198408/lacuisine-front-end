/**
 * Created by "Andy Van Camp" on 30/07/2021.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BoardAdminComponent} from './board-admin/board-admin.component';
import {HomeComponent} from './home/home.component';
import {ProfileComponent} from './profile/profile.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {OrderComponent} from './order/order.component';
import {NotAuthorizedComponent} from './shared/not-authorized/not-authorized.component';

import {PaymentFolderComponent} from './payment-folder/payment-folder.component';
import {UserOrdersComponent} from './user-orders/user-orders.component';
import {UserGuard} from './shared/guards/user.guard';
import {AdminGuard} from './shared/guards/admin.guard';
import {AdItemResolver, AdvertisementResolver} from './home/advertisement/advertisement-resolver.service';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, resolve: {advertisement: AdvertisementResolver, adItem: AdItemResolver}},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {
    path: 'profile', canActivate: [UserGuard], component: ProfileComponent, children: [
      {path: ':id/orders', component: UserOrdersComponent}
    ]
  },
  {path: 'restaurant', loadChildren: () => import('./restaurant/restaurant.module').then(m => m.RestaurantModule)},
  {path: 'admin', component: BoardAdminComponent, canActivate: [AdminGuard]},
  {path: 'order', component: OrderComponent},
  {path: 'notauthorized', component: NotAuthorizedComponent},
  {path: '**', component: PageNotFoundComponent},
  {path: 'paymentform', component: PaymentFolderComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes, {paramsInheritanceStrategy: 'always'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
